import { NavList } from "../atom/navList";

export const NavSec: React.FC<NavSecComponent> = (props) => {
    return (
        <>
            <ul className="list-none flex lg:space-x-4">
                {props.items.map((item, index) => (
                    <NavList key={index} text={item.text} status={item.status}/>
                ))}
            </ul>
        </>
    );
}