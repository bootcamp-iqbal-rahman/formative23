import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hook";
import { RootState } from "../../redux/store";
import { BottomCard } from "../atom/bottomCard"
import { fetchDataText } from "../../redux/action/action";

export const BottomBody: React.FC = () => {
    const dispatch = useAppDispatch();
    const data = useAppSelector((state: RootState) => state.texts.data);

    useEffect(() => {
        dispatch(fetchDataText());
    }, [])

    return (
        <>
            <div className="flex justify-center lg:max-h-max" id="bottom-body">
                <div className="mb-8 mx-10 lg:max-w-0 lg:flex justify-center lg:space-x-4 max-h-60">
                    {data.map((item, index) => (
                        <BottomCard 
                            key={index}
                            title={item}
                            desc={item}
                            button={item} />
                    ))}
                </div>
            </div>
        </>
    )
}