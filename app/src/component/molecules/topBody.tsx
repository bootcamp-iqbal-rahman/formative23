import { TopImg } from "../atom/topImg"
import { RootState } from "../../redux/store";
import { useEffect } from "react";
import { fetchDataImage } from "../../redux/action/action";
import { useAppDispatch, useAppSelector } from "../../redux/hook";

export const TopBody: React.FC = () => {
    const dispatch = useAppDispatch();
    const data = useAppSelector((state: RootState) => state.images.data);

    useEffect(() => {
        dispatch(fetchDataImage());
    }, [])

    return (
        <>
            <div id="top-body" className="mt-4 mx-10 lg:mt-8">
                <h1 className="text-center text-2xl lg:text-4xl">HOT SHIRTS FOR THIS MONTH</h1>
                <div className="my-8 grid justify-center space-y-4 lg:space-y-0 lg:space-x-4 lg:flex">
                    {data.map((item, index) => (
                        <TopImg 
                            key={index}
                            src={item} />
                    ))}
                </div>
            </div>
        </>
    )
}