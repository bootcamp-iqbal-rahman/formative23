import { Body } from "../organism/body";
import { Navbar } from "../organism/navbar"

export const HomePage: React.FC = () => {
    return (
        <div>
            <Navbar />
            <Body />

            
        </div>
    );
}