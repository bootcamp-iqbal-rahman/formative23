interface NavListComponent {
    text: string;
    status?: boolean;
}

interface NavSecComponent {
    items: NavListComponent[];
}

interface BottomCardComponent {
    title: string;
    desc: string;
    button: string;
}

interface Img {
    src: string;
}