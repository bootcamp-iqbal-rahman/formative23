export const BottomCard: React.FC<BottomCardComponent> = (props) => {
    return (
        <>
            <div className="mt-8 text-center flex flex-col justify-start">
                <h1 className="min-w-72 text-3xl">{props.title}</h1>
                <p className="text-white mt-4 p-2">
                    {props.desc}
                </p>
                <div className="mt-auto">
                    <button className="bg-black rounded-lg mt-auto max-w-40 justify-center">
                        {props.button}
                    </button>
                </div>
            </div>
        </>
    )
}