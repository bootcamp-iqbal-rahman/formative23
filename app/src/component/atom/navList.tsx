export const NavList: React.FC<NavListComponent> = (props) => {
    return (
        <>
            <li className={`inline-block lg:mr-4 pt-4 p-3 lg:p-7 ${props.status ? 'active' : ''}`}>
                <a href="#"><span>{props.text}</span></a> 
            </li>
        </>
    );
}