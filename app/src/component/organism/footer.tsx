import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hook";
import { RootState } from "../../redux/store";
import { TopImg } from "../atom/topImg"
import { fetchDataSocial } from "../../redux/action/action";

export const Footer: React.FC = () => {
    const dispatch = useAppDispatch();
    const data = useAppSelector((state: RootState) => state.socials.data);

    useEffect(() => {
        dispatch(fetchDataSocial());
    }, [])
    
    return (
        <div className="mt-8 lg:flex justify-center text-white" id="footer">
            <div className="m-8">
                <h1 className="drop-shadow-lg text-xl font-bold tracking-wide">GET WEEKLY NEWSLETTER</h1>
                <form className="mt-8">
                    <label className="sr-only">Email</label>
                    <input className="rounded-sm min-h-10" type="email" required></input>
                    <button className="bg-black rounded-md p-2" type="submit">Subscribe</button>
                </form>
            </div>
            <div className="m-8 max-w-96">
                <h1 className="drop-shadow-lg text-xl font-bold tracking-wide">LATEST POST</h1>
                <p className="mt-8">
                    This is just a place holder, so you can see what the site would look like. ...
                    <span className="float-right">12/05/2011</span>
                </p>
            </div>
            <div className="m-8">
                <h1 className="drop-shadow-lg text-xl font-bold tracking-wide">FOLLOW US</h1>
                <div className="flex justify-center space-x-4 mt-8">
                    {data.map((item, index) => (
                        <TopImg 
                            key={index}
                            src={item} />
                    ))}
                </div>
            </div>
        </div>
    )
}