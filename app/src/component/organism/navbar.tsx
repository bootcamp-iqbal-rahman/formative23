import { NavSec } from "../molecules/navSec";

export const Navbar: React.FC = () => {
    return (
        <div className="container mx-auto justify-center flex min-w-full min-h-20 lg:min-h-96 items-center lg:text-2xl text-white" id="header">
            <NavSec items={[
                {text:"Home", status:true},
                {text:"About"},
                {text:"Men"},
            ]} />
            <a className="mt-10 max-w-0 lg:max-w-64 lg:max-w-none">
                <img src="/images/logo.png"></img>
            </a>
            <NavSec items={[
                {text:"Women"},
                {text:"Blog"},
                {text:"Contact"},
            ]} />
        </div>
    );
}