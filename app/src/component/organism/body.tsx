import { BottomBody } from "../molecules/bottomBody";
import { TopBody } from "../molecules/topBody"
import { Footer } from "./footer";

export const Body: React.FC = () => {
    return (
        <>
            <div className="">
                <TopBody />
                <BottomBody />
                <Footer />
            </div>
        </>
    );
}