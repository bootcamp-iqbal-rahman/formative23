// rootReducer.js
import { combineReducers } from 'redux';
import imageReducer from './imageReducer';
import textReducer from './textReducer';
import socialReducer from './socialReducer';

export const rootReducer = combineReducers({
    images: imageReducer,
    texts: textReducer,
    socials: socialReducer
});
