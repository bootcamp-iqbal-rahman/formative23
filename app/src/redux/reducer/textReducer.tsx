import { 
    FETCH_TEXT_FAILURE,
    FETCH_TEXT_REQUEST,
    FETCH_TEXT_SUCCESS,
    FetchTextAction 
} from "../action/interfaceAction";


const initialState = {
    loading: false, data: [], error: ''
};

const textReducer = (state = initialState, action: FetchTextAction) => {
    switch (action.type) {
        case FETCH_TEXT_REQUEST:
            return {...state, data2: {...state, loading: true } };
        case FETCH_TEXT_SUCCESS:
            return {...state, data2: {...state, loading: false, data: action.payload, error: '' } };
        case FETCH_TEXT_FAILURE:
            return {...state, data2: {...state, loading: false, data: [], error: action.payload } };
        default:
            return state;
    }
};

export default textReducer;
