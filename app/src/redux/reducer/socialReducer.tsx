import { 
    FETCH_SOCIAL_FAILURE,
    FETCH_SOCIAL_REQUEST, 
    FETCH_SOCIAL_SUCCESS, 
    FetchSocialAction } from "../action/interfaceAction";

const initialState = {
    loading: false, data: [], error: ''
};

const socialReducer = (state = initialState, action: FetchSocialAction) => {
    switch (action.type) {
        case FETCH_SOCIAL_REQUEST:
            return {...state, data3: {...state, loading: true } };
        case FETCH_SOCIAL_SUCCESS:
            return {...state, data3: {...state, loading: false, data: action.payload, error: '' } };
        case FETCH_SOCIAL_FAILURE:
            return {...state, data3: {...state, loading: false, data: [], error: action.payload } };
        default:
            return state;
    }
};

export default socialReducer;
