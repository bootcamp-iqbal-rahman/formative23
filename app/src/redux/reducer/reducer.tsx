import {
    FETCH_IMAGE_REQUEST,
    FETCH_IMAGE_SUCCESS,
    FETCH_IMAGE_FAILURE,
    FETCH_TEXT_REQUEST,
    FETCH_TEXT_SUCCESS,
    FETCH_TEXT_FAILURE,
    FETCH_SOCIAL_REQUEST,
    FETCH_SOCIAL_SUCCESS,
    FETCH_SOCIAL_FAILURE,
} from "../action/interfaceAction"

const initialState = {
    images: { loading: false, data: [], error: '' },
    textses: { loading: false, data: [], error: '' },
    sociales: { loading: false, data: [], error: '' },
};

const reducer = (state = initialState, action: any) => {
    switch (action.type) {
        case FETCH_IMAGE_REQUEST:
            return { ...state, data1: { ...state.images, loading: true } };
        case FETCH_IMAGE_SUCCESS:
            return { ...state, data1: { loading: false, data: action.payload, error: '' } };
        case FETCH_IMAGE_FAILURE:
            return { ...state, data1: { loading: false, data: [], error: action.payload } };
        case FETCH_TEXT_REQUEST:
            return {...state, data2: {...state.textses, loading: true } };
        case FETCH_TEXT_SUCCESS:
            return {...state, data2: {...state.textses, loading: false, data: action.payload, error: '' } };
        case FETCH_TEXT_FAILURE:
            return {...state, data2: {...state.textses, loading: false, data: [], error: action.payload } };
        case FETCH_SOCIAL_REQUEST:
            return {...state, data3: {...state.sociales, loading: true } };
        case FETCH_SOCIAL_SUCCESS:
            return {...state, data3: {...state.sociales, loading: false, data: action.payload, error: '' } };
        case FETCH_SOCIAL_FAILURE:
            return {...state, data3: {...state.sociales, loading: false, data: [], error: action.payload } };
        default:
            return state;
    }
};

export default reducer;
