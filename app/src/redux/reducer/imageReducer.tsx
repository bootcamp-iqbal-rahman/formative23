import { 
    FETCH_IMAGE_FAILURE,
    FETCH_IMAGE_REQUEST, 
    FETCH_IMAGE_SUCCESS,
    FetchImageAction
} from "../action/interfaceAction";

const initialState = {
    loading: false, data: [], error: ''
};

const imageReducer = (state = initialState, action: FetchImageAction) => {
    switch (action.type) {
        case FETCH_IMAGE_REQUEST:
            return { ...state, data1: { ...state, loading: true } };
        case FETCH_IMAGE_SUCCESS:
            return { ...state, data1: { loading: false, data: action.payload, error: '' } };
        case FETCH_IMAGE_FAILURE:
            return { ...state, data1: { loading: false, data: [], error: action.payload } };
        default:
            return state;
    }
};

export default imageReducer;
