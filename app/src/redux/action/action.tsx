import axios from 'axios';
import { 
    FETCH_IMAGE_FAILURE,
    FETCH_IMAGE_REQUEST,
    FETCH_IMAGE_SUCCESS,
    FETCH_SOCIAL_FAILURE,
    FETCH_SOCIAL_REQUEST,
    FETCH_SOCIAL_SUCCESS,
    FETCH_TEXT_FAILURE,
    FETCH_TEXT_REQUEST,
    FETCH_TEXT_SUCCESS,
    FetchImageAction,
    FetchSocialAction,
    FetchTextAction
} from './interfaceAction';
import { Dispatch } from 'redux';

export const fetchDataImageRequest = () => {
    return {
        type: FETCH_IMAGE_REQUEST
    };
};

export const fetchDataImageSuccess = (data: FetchImage[]) => {
    return {
        type: FETCH_IMAGE_SUCCESS,
        payload: data
    };
};

export const fetchDataImageFailure = (error: any) => {
    return {
        type: FETCH_IMAGE_FAILURE,
        payload: error
    };
};

export const fetchDataTextRequest = () => {
    return {
        type: FETCH_TEXT_REQUEST
    }
}

export const fetchDataTextSuccess = (data: FetchText[]) => {
    return {
        type: FETCH_TEXT_SUCCESS,
        payload: data
    }
}

export const fetchDataTextFailure = (error: any) => {
    return {
        type: FETCH_TEXT_FAILURE,
        payload: error
    }
}

export const fetchDataSocialRequest = () => {
    return {
        type: FETCH_SOCIAL_REQUEST
    }
}

export const fetchDataSocialSuccess = (data: FetchSocial[]) => {
    return {
        type: FETCH_SOCIAL_SUCCESS,
        payload: data
    }
}

export const fetchDataSocialFailure = (error: any) => {
    return {
        type: FETCH_SOCIAL_FAILURE,
        payload: error
    }
}

export const fetchDataImage = () => {
    return (dispatch: Dispatch<FetchImageAction>) => {
        dispatch({ type: FETCH_IMAGE_REQUEST });
        axios.get('http://localhost:3000/image')
            .then(response => {
                dispatch({ type: FETCH_IMAGE_SUCCESS, payload: response.data });
            })
            .catch(error => {
                dispatch({ type: FETCH_IMAGE_FAILURE, payload: error.message});
            });
    };
};

export const fetchDataText = () => {
    return (dispatch: Dispatch<FetchTextAction>) => {
        dispatch({ type: FETCH_TEXT_REQUEST });
        axios.get('http://localhost:3000/text')
            .then(response => {
                dispatch({ type: FETCH_TEXT_SUCCESS, payload: response.data });
            })
            .catch(error => {
                dispatch({ type: FETCH_TEXT_FAILURE, payload: error.message});
            });
    };
}

export const fetchDataSocial = () => {
    return (dispatch: Dispatch<FetchSocialAction>) => {
        dispatch({ type: FETCH_SOCIAL_REQUEST });
        axios.get('http://localhost:3000/social')
            .then(response => {
                dispatch({ type: FETCH_SOCIAL_SUCCESS, payload: response.data });
            })
            .catch(error => {
                dispatch({ type: FETCH_SOCIAL_FAILURE, payload: error.message});
            });
    };
}