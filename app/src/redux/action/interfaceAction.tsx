export const FETCH_IMAGE_REQUEST = 'FETCH_IMAGE_REQUEST';
export const FETCH_IMAGE_SUCCESS = 'FETCH_IMAGE_SUCCESS';
export const FETCH_IMAGE_FAILURE = 'FETCH_IMAGE_FAILURE';
export const FETCH_TEXT_REQUEST = 'FETCH_TEXT_REQUEST';
export const FETCH_TEXT_SUCCESS = 'FETCH_TEXT_SUCCESS';
export const FETCH_TEXT_FAILURE = 'FETCH_TEXT_FAILURE';
export const FETCH_SOCIAL_REQUEST = 'FETCH_SOCIAL_REQUEST';
export const FETCH_SOCIAL_SUCCESS = 'FETCH_SOCIAL_SUCCESS';
export const FETCH_SOCIAL_FAILURE = 'FETCH_SOCIAL_FAILURE';

export interface FetchImageRequestAction {
    type: typeof FETCH_IMAGE_REQUEST;
}

export interface FetchImageSuccessAction {
    type: typeof FETCH_IMAGE_SUCCESS;
    payload: FetchImage[];
}

export interface FetchImageFailureAction {
    type: typeof FETCH_IMAGE_FAILURE;
    payload: string;
}

export interface FetchTextRequestAction {
    type: typeof FETCH_TEXT_REQUEST;
}

export interface FetchTextSuccessAction {
    type: typeof FETCH_TEXT_SUCCESS;
    payload: FetchText[];
}

export interface FetchTextFailureAction {
    type: typeof FETCH_TEXT_FAILURE;
    payload: string;
}

export interface FetchSocialRequestAction {
    type: typeof FETCH_SOCIAL_REQUEST;
}

export interface FetchSocialSuccessAction {
    type: typeof FETCH_SOCIAL_SUCCESS;
    payload: FetchSocial[];
}

export interface FetchSocialFailureAction {
    type: typeof FETCH_SOCIAL_FAILURE;
    payload: string;
}

export type FetchImageAction = FetchImageRequestAction | FetchImageSuccessAction | FetchImageFailureAction; 
export type FetchTextAction = FetchTextRequestAction | FetchTextSuccessAction | FetchTextFailureAction; 
export type FetchSocialAction = FetchSocialRequestAction | FetchSocialSuccessAction | FetchSocialFailureAction; 
