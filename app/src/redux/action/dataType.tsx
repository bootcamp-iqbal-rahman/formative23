interface FetchImage {
    id: string;
    src: string;
}

interface FetchText {
    id: string;
    title: string;
    desc: string;
    button: string;
}

interface FetchSocial {
    id: string;
    src: string;
}